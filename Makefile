obj-m += chardev.o

.PHONY: all run build kernel user clean
all: build

run: clean build
	-mknod /dev/char_dev c 100 0
	-insmod chardev.ko	
	./ioctl.o
	rmmod chardev
	dmesg | grep CharDev

build: kernel user

kernel:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules 

user: ioctl.c
	gcc $^ -g -o ioctl.o -static-libgcc

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
	rm -f ioctl.o

