
/*
 *  ioctl.c - the process to use ioctl's to control the kernel module
 *
 *  Until now we could have used cat for input and output.  But now
 *  we need to do ioctl's, which require writing our own process.
 */

/* 
 * device specifics, such as ioctl numbers and the
 * major device file. 
 */
#include "chardev.h"

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>		/* open */
#include <unistd.h>		/* exit */
#include <sys/ioctl.h>		/* ioctl */
#define MAJOR_NUM 100
#define IOCTL_GET_NTH_BYTE _IOWR(MAJOR_NUM, 2, int)

char *message = NULL;

char* halt()
{
	return message;
}

ioctl_experiment(int file_desc)
{
int i = 0;
	ioctl(file_desc,IOCTL_GET_NTH_BYTE,halt);
}
/* 
 * Main - Call the ioctl functions 
 */
main()
{
	int file_desc;
	file_desc = open(DEVICE_FILE_NAME, 0);
	if (file_desc < 0) {
		printf("Can't open device file: %s\n", DEVICE_FILE_NAME);
		perror("reason:");
		exit(-1);
	}
	message = malloc(sizeof("SUCCESS HEAP!") + 1);
	sprintf(message, "%s", "SUCCESS HEAP!");
	ioctl_experiment(file_desc);
	printf("%d\n", getpid());
	close(file_desc);
}
