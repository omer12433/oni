/*
 *  chardev.c - Create an input/output character device
 */
#include <linux/slab.h>
#include <asm/segment.h>
#include <linux/buffer_head.h>
#include <linux/kernel.h>	/* We're doing kernel work */
#include <linux/module.h>	/* Specifically, a module */
#include <linux/fs.h>
#include <asm/uaccess.h>	/* for get_user and put_user */
#include <linux/sched.h>	/* For current */
#include <linux/list.h>
#include <linux/elf.h>
#include <linux/path.h>
#include <linux/namei.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <asm/mman.h>
#include <linux/vmalloc.h>
#include "chardev.h"
#define SUCCESS 0
#define DEVICE_NAME "char_dev"
#define BUF_LEN 80
#define BUFFER_SIZE (0x1000)
/* 
 * Is the device open right now? Used to prevent
 * concurent access into the same device 
 */
static int Device_Open = 0;
static addresses_t *addresses_resolvers = NULL;
static int addresses_resolvers_count = 0;

typedef void *function_type(void);

/* 
 * The message the device will give when asked 
 */
static char Message[BUF_LEN];

/* 
 * How far did the process reading the message get?
 * Useful if the message is larger than the size of the
 * buffer we get to fill in device_read. 
 */
static char *Message_Ptr;
//static char *buf;

/* 
 * This is called whenever a process attempts to open the device file 
 */
/****** FILE FUNCTIONS ********/
struct file* file_open(const char* path, int flags, int rights) {
    struct file* filp = NULL;
    mm_segment_t oldfs;
    int err = 0;

    oldfs = get_fs();
    set_fs(get_ds());
    filp = filp_open(path, flags, rights);
    set_fs(oldfs);
    if(IS_ERR(filp)) {
        err = PTR_ERR(filp);
        return NULL;
    }
    return filp;
}

void file_close(struct file* file) {
    filp_close(file, NULL);
}

int file_read(struct file* file, unsigned long long offset, unsigned char* data, unsigned int size) {
    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(get_ds());
    ret = vfs_read(file, data, size, &offset);

    set_fs(oldfs);
    return ret;
}

int file_write(struct file* file, unsigned long long offset, unsigned char* data, unsigned int size) {
    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(get_ds());

    ret = vfs_write(file, data, size, &offset);

    set_fs(oldfs);
    return ret;
}
/****************************/


static int device_open(struct inode *inode, struct file *file)
{
#ifdef DEBUG
	printk(KERN_INFO "CharDev: device_open(%p)\n", file);
#endif

	/* 
	 * We don't want to talk to two processes at the same time 
	 */
	if (Device_Open)
		return -EBUSY;

	Device_Open++;
	/*
	 * Initialize the message 
	 */
	Message_Ptr = Message;
	try_module_get(THIS_MODULE);
	return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file)
{
#ifdef DEBUG
	
	printk(KERN_INFO "CharDev: device_release(%p,%p)\n", inode, file);
#endif

	/* 
	 * We're now ready for our next caller 
	 */
	Device_Open--;

	module_put(THIS_MODULE);
	return SUCCESS;
}

/* 
 * This function is called whenever a process which has already opened the
 * device file attempts to read from it.
 */
static ssize_t device_read(struct file *file,	/* see include/linux/fs.h   */
			   char __user * buffer,	/* buffer to be
							 * filled with data */
			   size_t length,	/* length of the buffer     */
			   loff_t * offset)
{
	/* 
	 * Number of bytes actually written to the buffer 
	 */
	int bytes_read = 0;

#ifdef DEBUG
	printk(KERN_INFO "CharDev: device_read(%p,%p,%d)\n", file, buffer, length);
#endif

	/* 
	 * If we're at the end of the message, return 0
	 * (which signifies end of file) 
	 */
	if (*Message_Ptr == 0)
		return 0;

	/* 
	 * Actually put the data into the buffer 
	 */
	while (length && *Message_Ptr) {

		/* 
		 * Because the buffer is in the user data segment,
		 * not the kernel data segment, assignment wouldn't
		 * work. Instead, we have to use put_user which
		 * copies data from the kernel data segment to the
		 * user data segment. 
		 */
		put_user(*(Message_Ptr++), buffer++);
		length--;
		bytes_read++;
	}

#ifdef DEBUG
	printk(KERN_INFO "CharDev: Read %d bytes, %d left\n", bytes_read, length);
#endif

	/* 
	 * Read functions are supposed to return the number
	 * of bytes actually inserted into the buffer 
	 */
	return bytes_read;
}

/* 
 * This function is called when somebody tries to
 * write into our device file. 
 */
static ssize_t
device_write(struct file *file,
	     const char __user * buffer, size_t length, loff_t * offset)
{
	int i;

#ifdef DEBUG
	printk(KERN_INFO "CharDev: device_write(%p,%s,%d)", file, buffer, length);
#endif

	for (i = 0; i < length && i < BUF_LEN; i++)
		get_user(Message[i], buffer + i);

	Message_Ptr = Message;

	/* 
	 * Again, return the number of input characters used 
	 */
	return i;
}
/*int* get_symbol_by_name(char * name)
{
	Elf32_Ehdr *header =  NULL;
	Elf32_Off sectab_offset = 0;	
	Elf32_Shdr *section_header = NULL;	
	int i = 0;

	header = (Elf32_Ehdr *)(buf);
	printk(KERN_INFO "CharDev: after header\n");
	sectab_offset = header->e_shoff;
	printk(KERN_INFO "CharDev: after offset %d\n",header->e_shoff);
	section_header = (Elf32_Shdr *)(buf + header->e_shoff);
	printk(KERN_INFO "CharDev: after section_header\n");
 	Elf32_Shdr *sh_strtab = &(section_header[header->e_shstrndx]);
  	printk(KERN_INFO "CharDev: after srttab");
	const char *const sh_strtab_p = buf + sh_strtab->sh_offset;
	printk(KERN_INFO "CharDev: after sh_strtab_p");
  	for (i = 0; i < header->e_shnum; ++i) {
    		printk(KERN_INFO "CharDev: %2d: %4d '%s'\n", i, section_header[i].sh_name,
           	sh_strtab_p + section_header[i].sh_name);
  	}




	return NULL;
}*/

inline unsigned long * solve_address(unsigned long *address)
{
	unsigned long *base = 0;
	int  i =0;

	for (i = 0; i < addresses_resolvers_count; i++)
	{
		if (addresses_resolvers[i].user_start <= address && address < addresses_resolvers[i].user_end)
		{
			return address - addresses_resolvers[i].user_start + addresses_resolvers[i].kernel_base;
		}
	}
	
	return address;
}

#if 0
unsigned long * correct_jump_or_call(unsigned long *buf, unsigned long offset,
	unsigned long *user_code_base)
{
	char priop = (char)*(buf + offset);
	/* Jump short */
	if (0x70 <= priop && priop <= 7F)
	{
	}

	switch ((char)(buf + offset))
	{

	}
}
#endif

void solve_addresses(unsigned long *buf, int user_code_start_address, int user_code_end_address)
{
	unsigned long *current_value = 0;
	unsigned long code_length = (unsigned long)(user_code_end_address - user_code_start_address);
	unsigned long *solved_address = 0;
	unsigned long i = 0;

	for (i = 0; i < code_length - 3; ++i, ++buf)
	{
		current_value = (unsigned long *)*buf;
		solved_address = solve_address(current_value);
		*buf = (unsigned long)current_value;			
	}
}

void get_section(unsigned long *buffer, int start_address, int end_address)
{
	int length = end_address - start_address;
	printk(KERN_INFO "CharDev: SECTION - user base: %p, kernel base: %p\n",
		start_address, buffer);
	copy_from_user(buffer,(void*)start_address, length);

	solve_addresses(buffer, start_address, end_address);
}

/* 
 * This function is called whenever a process tries to do an ioctl on our
 * device file. We get two extra parameters (additional to the inode and file
 * structures, which all device functions get): the number of the ioctl called
 * and the parameter given to the ioctl function.
 *
 * If the ioctl is write or read/write (meaning output is returned to the
 * calling process), the ioctl call returns the output of this function.
 *
 */
int device_ioctl(
    struct file *file,
    unsigned int ioctl_num,/* The number of the ioctl */
    unsigned long ioctl_param) /* The parameter to it */
{
	unsigned long *code = NULL, data = NULL, brk = NULL;
	addresses_t addresses_resolvers_arr[3];

	int code_length = current->mm->end_code - current->mm->start_code;
	int data_length = current->mm->end_data - current->mm->start_data;
	int brk_length = current->mm->brk - current->mm->start_brk;

	code = __vmalloc(code_length, GFP_KERNEL, PAGE_SHARED_EXEC);
	if( code == NULL)
	{
		goto Exit;
	}

	data = __vmalloc(data_length, GFP_KERNEL, PAGE_SHARED_EXEC);
	if( code == NULL)
	{
		goto Exit;
	}

	if (brk_length > 0)
	{
		brk = __vmalloc(brk_length, GFP_KERNEL, PAGE_SHARED_EXEC);
		if( brk == NULL)
		{
			goto Exit;
		}
	}

	addresses_resolvers_arr[0].kernel_base = code;
	addresses_resolvers_arr[0].user_start = current->mm->start_code;
	addresses_resolvers_arr[0].user_end = current->mm->end_code;

	addresses_resolvers_arr[1].kernel_base = data;
	addresses_resolvers_arr[1].user_start = current->mm->start_data;
	addresses_resolvers_arr[1].user_end = current->mm->end_data;	
 
	addresses_resolvers_arr[2].kernel_base = brk;
	addresses_resolvers_arr[2].user_start = current->mm->start_brk;
	addresses_resolvers_arr[2].user_end = current->mm->brk;	

	addresses_resolvers = addresses_resolvers_arr;
	addresses_resolvers_count = sizeof(addresses_resolvers_arr) / sizeof(addresses_resolvers_arr[0]);
	function_type *function = (function_type *)ioctl_param;

	
	printk(KERN_INFO "CharDev function resolved address: %p\n", function);
	printk(KERN_INFO "CharDev: start code: %p. end code: %p function addr :%x\n",
		current->mm->start_code, current->mm->end_code,ioctl_param);
	get_section(code, current->mm->start_code, current->mm->end_code);
	get_section(data, current->mm->start_data, current->mm->end_data);
	function = (function_type *)solve_address((long unsigned int *)function);

	printk(KERN_INFO "CharDev: first 4 bytes %4x, offset: 0x%x\n", *(unsigned long *)function, (unsigned long)function - (unsigned long)code);
	printk(KERN_INFO "CharDev: result: %s\n", function());

Exit:
	if(code != NULL)
	{
		vfree(code);
	}

	if (data != NULL)
	{
		vfree(data);
	}
	
	if (brk != NULL)
	{
		vfree(brk);
	}
 
	return SUCCESS;
}

/* Module Declarations */

/* 
 * This structure will hold the functions to be called
 * when a process does something to the device we
 * created. Since a pointer to this structure is kept in
 * the devices table, it can't be local to
 * init_module. NULL is for unimplemented functions. 
 */
struct file_operations Fops = {
	.read = device_read,
	.write = device_write,
	.unlocked_ioctl = device_ioctl,
	.open = device_open,
	.release = device_release,	/* a.k.a. close */
};

/* 
 * Initialize the module - Register the character device 
 */
int init_module()
{
	int ret_val;
	/* 
	 * Register the character device (atleast try) 
	 */
	ret_val = register_chrdev(MAJOR_NUM, DEVICE_NAME, &Fops);

	/* 
	 * Negative values signify an error 
	 */
	if (ret_val < 0) {
		printk(KERN_ALERT "%s failed with %d\n",
		       "Sorry, registering the character device ", ret_val);
		return ret_val;
	}

	printk(KERN_INFO "CharDev: %s The major device number is %d.\n",
	       "Registeration is a success", MAJOR_NUM);
	printk(KERN_INFO "CharDev: If you want to talk to the device driver,\n");
	printk(KERN_INFO "CharDev: you'll have to create a device file. \n");
	printk(KERN_INFO "CharDev: We suggest you use:\n");
	printk(KERN_INFO "CharDev: mknod %s c %d 0\n", DEVICE_FILE_NAME, MAJOR_NUM);
	printk(KERN_INFO "CharDev: The device file name is important, because\n");
	printk(KERN_INFO "CharDev: the ioctl program assumes that's the\n");
	printk(KERN_INFO "CharDev: file you'll use.\n");

	return 0;
}

/* 
 * Cleanup - unregister the appropriate file from /proc 
 */
void cleanup_module()
{
	int ret;

	/* 
	 * Unregister the device 
	 */
	unregister_chrdev(MAJOR_NUM, DEVICE_NAME);
}



#if 0
	printk(KERN_INFO "CharDev: %d\n",(current)->pid);
	snprintf(symlink_name, sizeof(symlink_name), "/proc/%d/exe", current->pid);
	printk(KERN_INFO "CharDev: curr");

	if (NULL == (symlink_fd = file_open(symlink_name, O_RDONLY, 0)))
	{
		printk(KERN_INFO "CharDev: OPEN FAILURE\n, cannot open %s\n", symlink_name);
		goto Exit;
	}
	
	kern_path(symlink_name, 0, &f_path);
	if(vfs_getattr(&f_path,&kst) !=0)
	{
		printk(KERN_INFO "CharDev: failure\n");
		goto Exit;
	}

	total_bytes = kst.size;
	printk(KERN_INFO "CharDev: file size:%s",kst.size);
	spin_lock_irqsave(&(symlink_fd->f_inode->i_lock), flags);
	total_bytes = symlink_fd->f_inode->i_blocks *( symlink_fd->f_inode->i_blkbits/8);
	printk(KERN_INFO "CharDev : %d,%d\n",symlink_fd->f_inode->i_blocks,symlink_fd->f_inode->i_blkbits);
	spin_unlock_irqrestore(&(symlink_fd->f_inode->i_lock), flags);
	if (NULL == (buf = vmalloc(total_bytes)))
	{
		printk(KERN_INFO "CharDev: allocation failure tried to allocate %d bytes\n",total_bytes);
		goto Exit;
	}
	
	printk(KERN_INFO "CharDev:READ %d/%d\n", file_read(symlink_fd, 0, buf, total_bytes),total_bytes);
	
	//get_symbol_by_name("symtab");
	/* 
	 * Switch according to the ioctl called 
	 */
	switch (ioctl_num) {
	case IOCTL_SET_MSG:
		/* 
		 * Receive a pointer to a message (in user space) and set that
		 * to be the device's message.  Get the parameter given to 
		 * ioctl by the process. 
		 */
		temp = (char *)ioctl_param;

		/* 
		 * Find the length of the message 
		 */
		get_user(ch, temp);
		for (i = 0; ch && i < BUF_LEN; i++, temp++)
			get_user(ch, temp);

		device_write(file, (char *)ioctl_param, i, 0);
		break;

	case IOCTL_GET_MSG:
		/* 
		 * Give the current message to the calling process - 
		 * the parameter we got is a pointer, fill it. 
		 */
		i = device_read(file, (char *)ioctl_param, 99, 0);

		/* 
		 * Put a zero at the end of the buffer, so it will be 
		 * properly terminated 
		 */
		put_user('\0', (char *)ioctl_param + i);
		break;

	case IOCTL_GET_NTH_BYTE:
		/* 
		 * This ioctl is both input (ioctl_param) and 
		 * output (the return value of this function) 
		 */
		return Message[ioctl_param];
		break;
	}
#endif
